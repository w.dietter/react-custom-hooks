export default function validateAuth(values) {
  let errors = {};
  //Email
  if (!values.email) {
    errors.email = "Required email";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }

  //Password
  if (!values.password) {
    errors.password = "Required password";
  } else if (values.password.length < 6) {
    errors.password = "Password must have more than 6 characters.";
  }
  return errors;
}
