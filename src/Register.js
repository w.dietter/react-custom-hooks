import React from "react";
import useFormValidation from "./useFormValidation";
import validateAuth from "./validateAuth";

const INITIAL_STATE = {
  email: "",
  password: ""
};

const Register = () => {
  const {
    handleChange,
    handleSubmit,
    handleBlur,
    errors,
    values
  } = useFormValidation(INITIAL_STATE, validateAuth);

  return (
    <div>
      <h1>Register</h1>
      <form onSubmit={handleSubmit}>
        <input
          onChange={handleChange}
          name="email"
          value={values.email}
          autoComplete="off"
          onBlur={handleBlur}
          placeholder="your email address"
        />
        <input
          onChange={handleChange}
          name="password"
          value={values.password}
          onBlur={handleBlur}
          type="password"
          placeholder="Choose a safe password"
        />
        <button type="submit">Submit</button>
        <p>
          {errors &&
            Object.keys(errors).map(error => (
              <p key={error}>{errors[error]}</p>
            ))}
        </p>
      </form>
    </div>
  );
};

export default Register;
