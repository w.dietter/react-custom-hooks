import React, { useState, useEffect } from "react";
import Register from "./Register";

const useFetch = (url, defaultResponse) => {
  const [data, setData] = useState(defaultResponse);

  const getData = async url => {
    try {
      const res = await fetch(url);
      const data = await res.json();
      setData({
        isLoading: false,
        data
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getData(url);
  }, [url]);
  return [data.data, data.isLoading];
};

const App = () => {
  const [data, isLoading] = useFetch(
    "https://jsonplaceholder.typicode.com/users",
    {
      isLoading: true,
      data: null
    }
  );

  if (!data || isLoading) {
    return <h1>Loading...</h1>;
  }

  return (
    <>
      {data.map(user => (
        <div key={user.id}>
          <h2>{user.name}</h2>
        </div>
      ))}
      <Register />
    </>
  );
};

export default App;
