import React, { useState, useEffect } from "react";

const useFormValidation = (initialState, validate) => {
  const [values, setValues] = useState(initialState);
  const [errors, setErrors] = useState({});
  const [isSubmitting, setSubmitting] = useState(false);

  useEffect(() => {
    if (isSubmitting) {
      const noErrors = Object.keys(errors).length === 0;
      if (noErrors) {
        console.log("valid form");
        setSubmitting(false);
      } else {
        console.log(errors);
        setSubmitting(false);
      }
    }
  }, [errors]);

  function handleChange(event) {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  }

  function handleSubmit(event) {
    event.preventDefault();
    const validationErrors = validate(values);
    setSubmitting(true);
    setErrors(validationErrors);
  }

  function handleBlur() {}

  return {
    handleChange,
    handleSubmit,
    handleBlur,
    values,
    errors
  };
};
export default useFormValidation;
